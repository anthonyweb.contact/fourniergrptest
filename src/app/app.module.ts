import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeViewComponent } from './home/views/home-view/home-view.component';
import { LoginComponent } from './home/components/login/login.component';
import { RegisterComponent } from './home/components/register/register.component';
import { HeaderComponent } from './home/components/header/header.component';
import { DashboardViewComponent } from './dashboard/views/dashboard-view/dashboard-view.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AuthentificationService} from "./home/services/auth/authentification.service";
import {AuthGuardService} from "./home/services/auth/auth-guard.service";
import {HttpClientModule} from "@angular/common/http";
import { DashboardComponent } from './dashboard/components/dashboard/dashboard.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeViewComponent,
    LoginComponent,
    RegisterComponent,
    HeaderComponent,
    DashboardViewComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule

  ],
  providers: [
    AuthentificationService,
    AuthGuardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
