import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeViewComponent} from "./home/views/home-view/home-view.component";
import {DashboardViewComponent} from "./dashboard/views/dashboard-view/dashboard-view.component";
import {AuthGuardService} from "./home/services/auth/auth-guard.service";
import {LoginComponent} from "./home/components/login/login.component";
import {RegisterComponent} from "./home/components/register/register.component";

const routes: Routes = [
  { path: '', redirectTo: '/home/(auth:login)', pathMatch: 'full'},
  { path: 'dashboard', component: DashboardViewComponent, canActivate: [AuthGuardService], title: 'Dashboard' },
  { path: 'home', component: HomeViewComponent, title: 'Connexion/Inscription',
    children: [
      {path: 'login', component: LoginComponent, outlet: 'auth'},
      {path: 'register', component: RegisterComponent, outlet: 'auth'},
    ]
  },

];
// http://localhost:4200/#/home/(auth:login)
@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true, relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
