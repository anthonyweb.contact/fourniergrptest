import {TestBed} from '@angular/core/testing';
import {HttpClientModule} from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';
import {Router} from '@angular/router';

import {AuthentificationService} from './authentification.service';
import {AuthGuardService} from './auth-guard.service';

describe('AuthGuardService', () => {
  let service: AuthGuardService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        RouterTestingModule
      ],
      providers: [
        AuthentificationService
      ]
    });
    service = TestBed.inject(AuthGuardService);
  });


  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should check is user is loged to access this url and denied it since not', () => {
    const isLoggedInSpy = jest.spyOn(TestBed.inject(AuthentificationService), 'isLoggedIn').mockReturnValue(false);
    const navigateByUrlSpy = jest.spyOn(TestBed.inject(Router), 'navigateByUrl').mockImplementation();

    expect(service.canActivate()).toEqual(false);

    expect(isLoggedInSpy).toBeCalled();
    expect(navigateByUrlSpy).toBeCalledWith('/home/(auth:login)');
  });

  it('should check is user is logged to access this url and allowed it since yes', () => {
    const isLoggedInSpy = jest.spyOn(TestBed.inject(AuthentificationService), 'isLoggedIn').mockReturnValue(true);

    expect(service.canActivate()).toEqual(true);

    expect(isLoggedInSpy).toBeCalled();
  });
});
