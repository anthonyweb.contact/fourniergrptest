import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {BehaviorSubject} from "rxjs";


@Injectable()
export class AuthentificationService {
  private _uriR = 'http://restapi.adequateshop.com/api/authaccount/registration';
  private _uriL = 'http://restapi.adequateshop.com/api/authaccount/login';
  private _token: string;
  private _activeUser$: BehaviorSubject<string> = new BehaviorSubject(null);

  constructor(
    private _http: HttpClient,
    private _router: Router) {
  }

  // Arguments : ------
  // Résultat : Vérifie que la connexion n'est pas expirée.
  public isLoggedIn(): boolean {
    return !!this._getToken();
  }


  // Arguments : ------
  // Résultat : Renvoie le nom de l'utilisateur actif
  public getUserName(): BehaviorSubject<string> {
    return this._activeUser$;
  }


  // Arguments : Utilisateur
  // Résultat : Permet d'enregistrer l'utilisateur via une API
  public registerNewUser(user) {
    // Soucis rencontré avec l'API et son CORS (Access-Control-Allow-Origin), impossible de communiquer avec

    // this._http.post(`${this._uriR}`, user).subscribe((res: any) => {
    //   if (res) {
    //     this._saveToken(res.data.Token);
    //     this._activeUser$.next(res.data.Name);
    //     this._router.navigateByUrl('/dashboard');
    //   }
    // });
    const res = {
      "$id": "1",
      "code": 0,
      "message": "success",
      "data": {
        "$id": "2",
        "Id": 7075,
        "Name": "John",
        "Email": "John.Doe@gmail.fr",
        "Token": "3030401c-c5a5-43c8-8b73-2ab9e6f2ca22"
      }
    }
    this._saveToken(res.data.Token);
    this._activeUser$.next(res.data.Name);
    this._router.navigateByUrl('/dashboard');
  }


  // Arguments : Utilisateur
  // Résultat : Permet de connecter l'utilisateur via une API et récupérer ses informations
  public loginUser(user) {
    // Soucis rencontré avec l'API et son CORS (Access-Control-Allow-Origin), impossible de communiquer avec

    // this._http.get(`${this._uriL}`, {headers: {authorization: `Bearer ${this._getToken()}`}}).subscribe((data: any) => {
    //  if (data) {
    //    this._saveToken(res.data.Token);
    //    this._activeUser$.next(res.data.Name);
    //    this._router.navigateByUrl('/dashboard');
    //  }
    // });
    const res = {
      "$id": "1",
      "code": 0,
      "message": "success",
      "data": {
        "$id": "2",
        "Id": 7075,
        "Name": "John",
        "Email": "John.Doe@gmail.fr",
        "Token": "3030401c-c5a5-43c8-8b73-2ab9e6f2ca22"
      }
    }
    this._saveToken(res.data.Token);
    this._activeUser$.next(res.data.Name);
    this._router.navigateByUrl('/dashboard');
  }


  // Arguments : ------
  // Résultat : Déconnecte l'utilisateur, supprime ses infos et redirige vers la page de connexion
  public logout() {
    this._token = null;
    this._activeUser$.next(null);
    window.localStorage.removeItem('token');
    this._router.navigateByUrl('/home/(auth:login)');
  }


  // Arguments : ------
  // Résultat  : Renvoie le token de connexion s'il existe
  private _getToken(): string {
    if (!this._token) {
      this._token = localStorage.getItem('token');
    }
    return this._token;
  }


  // Arguments : Token
  // Résultat : Sauvegarde le token dans un localStorage
  private _saveToken(token: string): void {
    localStorage.setItem('token', token);
    this._token = token;
  }
}
