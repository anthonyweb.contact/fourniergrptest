import {TestBed} from '@angular/core/testing';
import {HttpClientModule} from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';


import {AuthentificationService} from './authentification.service';
import {BehaviorSubject} from "rxjs";
import {User} from "../../interfaces/userdetails";
import {Router} from "@angular/router";

describe('AuthentificationService', () => {
  let service: AuthentificationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        RouterTestingModule
      ],
      providers: [
        AuthentificationService
      ]
    });
    service = TestBed.inject(AuthentificationService);
  });


  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should check if user is looged', () => {
    const tokenMock = '62c44d88c5b36e2ed0545488';
    const _getTokenSpy = jest.spyOn<any, any>(service, '_getToken').mockReturnValue(tokenMock);

    expect(service.isLoggedIn()).toEqual(true);

    expect(_getTokenSpy).toBeCalled();
  });

  it('should get active user\s name', () => {
    const userNameMock = 'John Doe';
    service['_activeUser$'] = new BehaviorSubject<string>(userNameMock);

    expect(service.getUserName().value).toEqual(userNameMock);
  });

  it('should send post request to register and change url if succeed', () => {
    const userMock = <User>{}
    const resMock = {
      "$id": "1",
      "code": 0,
      "message": "success",
      "data": {
        "$id": "2",
        "Id": 7075,
        "Name": "John",
        "Email": "John.Doe@gmail.fr",
        "Token": "3030401c-c5a5-43c8-8b73-2ab9e6f2ca22"
      }
    }
    const _saveTokenSpy = jest.spyOn<any, any>(service, '_saveToken');
    const navigateByUrlSpy = jest.spyOn(TestBed.inject(Router), 'navigateByUrl').mockImplementation();

    service.registerNewUser(userMock)

    expect(_saveTokenSpy).toBeCalledWith(resMock.data.Token);
    expect(service['_activeUser$'].value).toEqual(resMock.data.Name);
    expect(navigateByUrlSpy).toBeCalledWith('/dashboard');
  });

  it('should send post request to login and change url if succeed', () => {
    const userMock = <User>{}
    const resMock = {
      "$id": "1",
      "code": 0,
      "message": "success",
      "data": {
        "$id": "2",
        "Id": 7075,
        "Name": "John",
        "Email": "John.Doe@gmail.fr",
        "Token": "3030401c-c5a5-43c8-8b73-2ab9e6f2ca22"
      }
    }
    const _saveTokenSpy = jest.spyOn<any, any>(service, '_saveToken');
    const navigateByUrlSpy = jest.spyOn(TestBed.inject(Router), 'navigateByUrl').mockImplementation();

    service.loginUser(userMock)

    expect(_saveTokenSpy).toBeCalledWith(resMock.data.Token);
    expect(service['_activeUser$'].value).toEqual(resMock.data.Name);
    expect(navigateByUrlSpy).toBeCalledWith('/dashboard');
  });

  it('should logout', () => {
    const navigateByUrlSpy = jest.spyOn(TestBed.inject(Router), 'navigateByUrl').mockImplementation();
    const removeItemSpy = jest.spyOn(Storage.prototype, 'removeItem');

    service.logout()

    expect(service['_token']).toEqual(null);
    expect(service['_activeUser$'].value).toEqual(null);
    expect(removeItemSpy).toBeCalledWith('token');
    expect(navigateByUrlSpy).toBeCalledWith('/home/(auth:login)');
  });

  it('should load token from localstorage', () => {
    const tokenMock = 'tokenmock'
    localStorage.setItem('token', tokenMock);

    expect(service['_getToken']()).toEqual(tokenMock)
  });

  it('should load token from service', () => {
    const tokenMock = 'tokenmock'
    service['_token'] = tokenMock;

    expect(service['_getToken']()).toEqual(tokenMock)
  });

  it('should save token', () => {
    const tokenMock = 'tokenmock'
    const setItemSpy = jest.spyOn(Storage.prototype, 'setItem');

    service['_saveToken'](tokenMock)
    expect(setItemSpy).toBeCalledWith('token', tokenMock);
    expect(service['_token']).toEqual(tokenMock);
  });
})
