import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

import { AuthentificationService } from './authentification.service';

@Injectable({
  providedIn: 'root'
})

export class AuthGuardService implements CanActivate {

  constructor(
    private _authService: AuthentificationService,
    private _router: Router) { }

  // Arguments : ------
  // Résultat : Vérifie que l'utilisateur est connecté via le service d'authentification
  public canActivate(): boolean {
    if (!this._authService.isLoggedIn()) {
      this._router.navigateByUrl('/home/(auth:login)');
      return false;
    }
    return true;
  }
}
