export interface TokenPayload {
  email: string;
  password?: string;
}

export interface User {
  name: string,
  email: string;
  password?: string;
}
