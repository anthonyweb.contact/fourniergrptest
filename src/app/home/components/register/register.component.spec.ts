import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterComponent } from './register.component';
import {AuthentificationService} from "../../services/auth/authentification.service";
import {HttpClientModule} from "@angular/common/http";
import {RouterTestingModule} from "@angular/router/testing";
import {ReactiveFormsModule} from "@angular/forms";

describe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        RouterTestingModule,
        ReactiveFormsModule
      ],
      declarations: [ RegisterComponent ],
      providers: [
        AuthentificationService
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should test register init form\'s values', () => {
    const registerForm = component.registerForm
    const credentials = {
      name: null,
      email: null,
      password: null
    };

    expect(registerForm.value).toEqual(credentials);
  });

  it('should test register form valid name field\'s value and validation', () => {
    const register: HTMLInputElement = fixture.debugElement.nativeElement
      .querySelector('#registerForm').querySelectorAll('input')[0];
    register.value = 'John Doe';
    register.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    fixture.whenStable().then(()=> {
      const nameValue = component.registerForm.get('name');
      expect(register.value).toEqual(nameValue.value);
      expect(nameValue.errors).toBeNull()
    })
  });

  it('should test register form invalid name field\'s value and validation', () => {
    const register: HTMLInputElement = fixture.debugElement.nativeElement
      .querySelector('#registerForm').querySelectorAll('input')[0];
    register.value = 'J';
    register.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    fixture.whenStable().then(()=> {
      const nameValue = component.registerForm.get('name');
      expect(register.value).toEqual(nameValue.value);
      expect(nameValue.errors).not.toBeNull()
    })
  });

  it('should test register form valid email field\'s value and validation', () => {
    const registerFormEmail: HTMLInputElement = fixture.debugElement.nativeElement
      .querySelector('#registerForm').querySelectorAll('input')[1];
    registerFormEmail.value = 'john.doe@gmail.fr';
    registerFormEmail.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    fixture.whenStable().then(()=> {
      const emailValue = component.registerForm.get('email');
      expect(registerFormEmail.value).toEqual(emailValue.value);
      expect(emailValue.errors).toBeNull()
    })
  });

  it('should test register form invalid email field\'s value and validation', () => {
    const registerFormEmail: HTMLInputElement = fixture.debugElement.nativeElement
      .querySelector('#registerForm').querySelectorAll('input')[1];
    registerFormEmail.value = 'johndoegmail.fr';
    registerFormEmail.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    fixture.whenStable().then(()=> {
      const emailValue = component.registerForm.get('email');
      expect(registerFormEmail.value).toEqual(emailValue.value);
      expect(emailValue.errors).not.toBeNull()
    })
  });

  it('should test register form valid password field\'s value and validation', () => {
    const registerForm: HTMLInputElement = fixture.debugElement.nativeElement
      .querySelector('#registerForm').querySelectorAll('input')[2];
    registerForm.value = '12346789';
    registerForm.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    fixture.whenStable().then(()=> {
      const passwordValue = component.registerForm.get('password');
      expect(registerForm.value).toEqual(passwordValue.value);
      expect(passwordValue.errors).toBeNull()
    })
  });

  it('should test register form invalid password field\'s value and validation', () => {
    const registerForm: HTMLInputElement = fixture.debugElement.nativeElement
      .querySelector('#registerForm').querySelectorAll('input')[2];
    registerForm.value = '16789';
    registerForm.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    fixture.whenStable().then(()=> {
      const passwordValue = component.registerForm.get('password');
      expect(registerForm.value).toEqual(passwordValue.value);
      expect(passwordValue.errors).not.toBeNull()
    })
  });

  it('should register the user', () => {
    const credentialsMock = {
      name: 'John Doe',
      email: "john.doe@gmail.fr",
      password: "123456789"
    }
    const registerFormName: HTMLInputElement = fixture.debugElement.nativeElement
      .querySelector('#registerForm').querySelectorAll('input')[0];
    const registerFormEmail: HTMLInputElement = fixture.debugElement.nativeElement
      .querySelector('#registerForm').querySelectorAll('input')[1];
    const registerFormPassword: HTMLInputElement = fixture.debugElement.nativeElement
      .querySelector('#registerForm').querySelectorAll('input')[2];
    registerFormName.value = credentialsMock.name;
    registerFormEmail.value = credentialsMock.email;
    registerFormPassword.value = credentialsMock.password;
    registerFormName.dispatchEvent(new Event('input'));
    registerFormEmail.dispatchEvent(new Event('input'));
    registerFormPassword.dispatchEvent(new Event('input'));
    const registerNewUserSpy = jest.spyOn(TestBed.inject(AuthentificationService), 'registerNewUser').mockImplementation();

    component.credentials = credentialsMock;
    component.onSubmit();

    expect(component.registerForm.valid).toBeTruthy();
    expect(registerNewUserSpy).toBeCalledWith(credentialsMock);
  });
});
