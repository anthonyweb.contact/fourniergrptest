import { Component, OnInit } from '@angular/core';
import {User} from "../../interfaces/userdetails";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthentificationService} from "../../services/auth/authentification.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.sass']
})
export class RegisterComponent implements OnInit {
  public registerForm: FormGroup;
  public credentials: User = {
    name: null,
    email: null,
    password: null
  };

  constructor(
    private _authService: AuthentificationService
  ) { }

  ngOnInit(): void {
    this.registerForm = new FormGroup({
      name: new FormControl(this.credentials.name, [
        Validators.required,
        Validators.minLength(2)
      ]),
      email: new FormControl(this.credentials.email, [
        Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$"),
        Validators.minLength(10),
        Validators.required
      ]),
      password: new FormControl(this.credentials.password, [
        Validators.required,
        Validators.minLength(6)
      ]),
    });
  }

  // Arguments : ------
  // Résultat  : Vérifie si le formulaire est valide et enregistre l'utilisateur
  onSubmit() {
    if(this.registerForm.valid) {
      this._authService.registerNewUser(this.credentials)
    }
  }
}
