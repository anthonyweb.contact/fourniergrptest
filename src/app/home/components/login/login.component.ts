import {Component, OnInit} from '@angular/core';
import {TokenPayload} from "../../interfaces/userdetails";
import {AuthentificationService} from "../../services/auth/authentification.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;
  public credentials: TokenPayload = {
    email: null,
    password: null
  };
  constructor(
    private _authService: AuthentificationService
  ) { }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      email: new FormControl(this.credentials.email, [
        Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$"),
        Validators.minLength(10),
        Validators.required
      ]),
      password: new FormControl(this.credentials.password, [
        Validators.required,
        Validators.minLength(6)
      ]),
    });
  }

  // Arguments : ------
  // Résultat  : Vérifie si le formulaire est valide et connecte l'utilisateur
  public login(){
    if(this.loginForm.valid){
      this._authService.loginUser(this.credentials);
    }
  }
}
