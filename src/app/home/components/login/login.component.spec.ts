import {ComponentFixture, TestBed} from '@angular/core/testing';

import {LoginComponent} from './login.component';
import {AuthentificationService} from "../../services/auth/authentification.service";
import {HttpClientModule} from "@angular/common/http";
import {ReactiveFormsModule} from "@angular/forms";
import {RouterTestingModule} from "@angular/router/testing";

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        RouterTestingModule,
        ReactiveFormsModule
      ],
      declarations: [LoginComponent],
      providers: [
        AuthentificationService
      ]
    })
      .compileComponents();

    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should test login init form\'s values', () => {
    const loginForm = component.loginForm
    const credentials = {
      email: null,
      password: null
    };

    expect(loginForm.value).toEqual(credentials);
  });

  it('should test valid email field\'s value and validation', () => {
    const loginFormEmail: HTMLInputElement = fixture.debugElement.nativeElement
      .querySelector('#loginForm').querySelectorAll('input')[0];
    loginFormEmail.value = 'john.doe@gmail.fr';
    loginFormEmail.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    fixture.whenStable().then(()=> {
      const emailValue = component.loginForm.get('email');
      expect(loginFormEmail.value).toEqual(emailValue.value);
      expect(emailValue.errors).toBeNull()
    })
  });

  it('should test invalid email field\'s value and validation', () => {
    const loginFormEmail: HTMLInputElement = fixture.debugElement.nativeElement
      .querySelector('#loginForm').querySelectorAll('input')[0];
    loginFormEmail.value = 'johndoegmail.fr';
    loginFormEmail.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    fixture.whenStable().then(()=> {
      const emailValue = component.loginForm.get('email');
      expect(loginFormEmail.value).toEqual(emailValue.value);
      expect(emailValue.errors).not.toBeNull()
    })
  });

  it('should test valid password field\'s value and validation', () => {
    const loginForm: HTMLInputElement = fixture.debugElement.nativeElement
      .querySelector('#loginForm').querySelectorAll('input')[1];
    loginForm.value = '12346789';
    loginForm.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    fixture.whenStable().then(()=> {
      const passwordValue = component.loginForm.get('password');
      expect(loginForm.value).toEqual(passwordValue.value);
      expect(passwordValue.errors).toBeNull()
    })
  });

  it('should test invalid password field\'s value and validation', () => {
    const loginForm: HTMLInputElement = fixture.debugElement.nativeElement
      .querySelector('#loginForm').querySelectorAll('input')[1];
    loginForm.value = '16789';
    loginForm.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    fixture.whenStable().then(()=> {
      const passwordValue = component.loginForm.get('password');
      expect(loginForm.value).toEqual(passwordValue.value);
      expect(passwordValue.errors).not.toBeNull()
    })
  });

  it('should login the user', () => {
    const credentialsMock = {
      email: "john.doe@gmail.fr",
      password: "123456789"
    }
    const loginFormEmail: HTMLInputElement = fixture.debugElement.nativeElement
      .querySelector('#loginForm').querySelectorAll('input')[0];
    const loginFormPassword: HTMLInputElement = fixture.debugElement.nativeElement
      .querySelector('#loginForm').querySelectorAll('input')[1];
    loginFormEmail.value = credentialsMock.email;
    loginFormPassword.value = credentialsMock.password;
    loginFormEmail.dispatchEvent(new Event('input'));
    loginFormPassword.dispatchEvent(new Event('input'));
    const loginUserSpy = jest.spyOn(TestBed.inject(AuthentificationService), 'loginUser').mockImplementation();

    component.credentials = credentialsMock;
    component.login();

    expect(component.loginForm.valid).toBeTruthy();
    expect(loginUserSpy).toBeCalledWith(credentialsMock);
  });
});
