import {Component, OnInit} from '@angular/core';
import {AuthentificationService} from "../../services/auth/authentification.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {
  public userName: string;

  constructor(private _authService: AuthentificationService) {}

  ngOnInit(): void {
    this._authService.getUserName().subscribe((userName: string) => {
      this.userName = userName;
    });
  }

  // Arguments : ------
  // Résultat  : Permet de déconnecter l'utilisateur
  public logout() {
    this._authService.logout();
  }
}
