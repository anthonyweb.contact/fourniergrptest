import { ComponentFixture, TestBed } from '@angular/core/testing';
import {HttpClientModule} from '@angular/common/http';

import { HeaderComponent } from './header.component';
import {AuthentificationService} from "../../services/auth/authentification.service";
import {BehaviorSubject} from "rxjs";
import {RouterTestingModule} from "@angular/router/testing";

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        RouterTestingModule
      ],
      declarations: [ HeaderComponent ],
      providers: [
        AuthentificationService
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should init component', () => {
    const userNameMock = "John Doe";
    const getUserNameSpy = jest.spyOn(TestBed.inject(AuthentificationService), 'getUserName').mockReturnValue(new BehaviorSubject(userNameMock));

    component.ngOnInit();

    expect(getUserNameSpy).toBeCalled();
    expect(component.userName).toEqual(userNameMock);
  });

  it('should logout the user', () => {
    const logoutSpy = jest.spyOn(TestBed.inject(AuthentificationService), 'logout').mockImplementation();

    component.logout();

    expect(logoutSpy).toBeCalled();
  });
});
